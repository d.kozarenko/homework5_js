"use strict";
function createNewUser() {
  let userFirstName = prompt("Enter your name please: ");
  let userLastName = prompt("Enter your last name please: ");
  let userBirthday = prompt("Enter your date of birth (dd.mm.yyyy): ");
  let newUser = {
    _firstName: userFirstName,
    _lastName: userLastName,
    _birthday: getDate(userBirthday),
    getAge: function () {
      let currentYear = new Date().getFullYear();
      let difference = currentYear - new Date(this._birthday).getFullYear();
      return difference;
    },
    getPassword: function () {
      return this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + new Date(this._birthday).getFullYear();
    },
  }
  return newUser;
};
const newUser = createNewUser();
newUser.getLogin = function () {
  return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
};
function getDate(date) {
  let d = date.split('.');
  if (typeof d[2] !== "undefined") {
    date = d[2] + '-' + d[1] + '-' + d[0];
    return date;
  }
  return 0;
};
Object.defineProperties(newUser, {
  firstName: {
    setFirstName: function (value) {
      this._firstName = value;
    },
    get: function () {
      return this._firstName;
    },
  },
  lastName: {
    setLastName: function (value) {
      this._lastName = value;
    },
    get: function () {
      return this._lastName;
    },
  },
  birthday: {
    setBirthday: function (value) {
      this._birthday = value;
    },
    get: function () {
      return this._birthday;
    },
  },
});
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);